/**
 * @author Edouard DUPIN
 * 
 * @copyright 2011, Edouard DUPIN, all right reserved
 * 
 * @license APACHE v2.0 (see license file)
 */

#ifndef __GALE_KEY_H__
#define __GALE_KEY_H__

#include <gale/key/keyboard.h>
#include <gale/key/Special.h>
#include <gale/key/status.h>
#include <gale/key/type.h>

#endif


