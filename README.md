gale
====

`gale` is an abtracter of the low level graphic windowing (like the SDL but in C++11)

It is designed to abstract systems
  - Linux
  - MacOs
  - Windows
  - Android
  - Ios

Release (master)
----------------

[![Build Status](https://travis-ci.org/atria-soft/gale.svg?branch=master)](https://travis-ci.org/atria-soft/gale)
[![Coverage Status](http://atria-soft.com/ci/coverage/atria-soft/gale.svg?branch=master)](http://atria-soft.com/ci/atria-soft/gale)
[![Test Status](http://atria-soft.com/ci/test/atria-soft/gale.svg?branch=master)](http://atria-soft.com/ci/atria-soft/gale)
[![Warning Status](http://atria-soft.com/ci/warning/atria-soft/gale.svg?branch=master)](http://atria-soft.com/ci/atria-soft/gale)

[![Build Status](http://atria-soft.com/ci/build/atria-soft/gale.svg?branch=master&tag=Linux)](http://atria-soft.com/ci/atria-soft/gale)
[![Build Status](http://atria-soft.com/ci/build/atria-soft/gale.svg?branch=master&tag=MacOs)](http://atria-soft.com/ci/atria-soft/gale)
[![Build Status](http://atria-soft.com/ci/build/atria-soft/gale.svg?branch=master&tag=Mingw)](http://atria-soft.com/ci/atria-soft/gale)

[![Build Status](http://atria-soft.com/ci/build/atria-soft/gale.svg?branch=master&tag=Android)](http://atria-soft.com/ci/atria-soft/gale)
[![Build Status](http://atria-soft.com/ci/build/atria-soft/gale.svg?branch=master&tag=IOs)](http://atria-soft.com/ci/atria-soft/gale)

Developement (dev)
------------------

[![Build Status](https://travis-ci.org/atria-soft/gale.svg?branch=dev)](https://travis-ci.org/atria-soft/gale)
[![Coverage Status](http://atria-soft.com/ci/coverage/atria-soft/gale.svg?branch=dev)](http://atria-soft.com/ci/atria-soft/gale)
[![Test Status](http://atria-soft.com/ci/test/atria-soft/gale.svg?branch=dev)](http://atria-soft.com/ci/atria-soft/gale)
[![Warning Status](http://atria-soft.com/ci/warning/atria-soft/gale.svg?branch=dev)](http://atria-soft.com/ci/atria-soft/gale)

[![Build Status](http://atria-soft.com/ci/build/atria-soft/gale.svg?branch=dev&tag=Linux)](http://atria-soft.com/ci/atria-soft/gale)
[![Build Status](http://atria-soft.com/ci/build/atria-soft/gale.svg?branch=dev&tag=MacOs)](http://atria-soft.com/ci/atria-soft/gale)
[![Build Status](http://atria-soft.com/ci/build/atria-soft/gale.svg?branch=dev&tag=Mingw)](http://atria-soft.com/ci/atria-soft/gale)

[![Build Status](http://atria-soft.com/ci/build/atria-soft/gale.svg?branch=dev&tag=Android)](http://atria-soft.com/ci/atria-soft/gale)
[![Build Status](http://atria-soft.com/ci/build/atria-soft/gale.svg?branch=dev&tag=IOs)](http://atria-soft.com/ci/atria-soft/gale)



Instructions
============


License (APACHE v2.0)
=====================
Copyright gale Edouard DUPIN

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


# gale
(APACHE-2) Graphic Abstraction Layer for Ewol: abstract the OpenGL/... interface and mouse/touch/keyboard for multiple system (Linux, Android, MacOs, Windows, Ios)
